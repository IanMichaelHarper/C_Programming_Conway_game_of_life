#include <stdio.h>
#include <stdlib.h>

int n = 40;	
int i,j;
int iterations = 1000;

void print_grid(int ** grid)
{
	for (i=0; i<n; i++)
	{
		for (j=0; j<n; j++)
		{
			if( grid[i][j] == 1) 
				printf("\x1b[31m%d\x1b[0m", grid[i][j]); //print colour
			else
				printf("%d", grid[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

int sum_num_alive(int **grid, int i, int j)
{
	int num_alive = 0;
	if (grid[(i-1+n)%n][j] == 1)
		num_alive++;
	if (grid[(i+1)%n][j] == 1)
		num_alive++;
	if (grid[i][(j-1+n)%n] == 1)
		num_alive++;
	if (grid[i][(j+1)%n] == 1)
		num_alive++;
	if (grid[(i-1+n)%n][(j-1+n)%n] == 1)
		num_alive++;
	if (grid[(i-1+n)%n][(j+1)%n] == 1)
		num_alive++;
	if (grid[(i+1)%n][(j-1+n)%n] == 1)
		num_alive++;
	if (grid[(i+1)%n][(j+1)%n] == 1)
		num_alive++;

	return num_alive;
}

int main()
{
	//create grid
	int ** grid = malloc(n * sizeof(int *));
	int * grid_cols = malloc(n * n * sizeof(int));
	for (i=0; i<n; i++)
	{
		grid[i] = &grid_cols[i*n];
		for (j=0; j<n; j++)
			grid[i][j] = 0;
	}

	//create new grid to replace old grid
	int ** new_grid = malloc(n * sizeof(int *));
	int * new_grid_cols = malloc(n * n * sizeof(int));
	for (i=0; i<n; i++)
	{
		new_grid[i] = &new_grid_cols[i*n];
		for (j=0; j<n; j++)
			new_grid[i][j] = 0;
	}

	int **temp;

	srand(time(NULL));

	//grid[i][j] = 2*drand48(); 
	
	//random initial conditions
	for (i=0; i<n; i++)
	{
		for (j=0; j<n; j++)
		{
			grid[i][j] = rand() % 2;
			new_grid[i][j] = grid[i][j];
		}
	}

	print_grid(grid);

	//game of life loop
	int iter;
	for (iter=0; iter<iterations; iter++)
	{
		for (i=0; i<n; i++)
		{
			for (j=0; j<n; j++)
			{

				if (grid[i][j] == 0)
				{
					int num_alive = sum_num_alive(grid, i, j);
					if (num_alive == 3)
						new_grid[i][j] = 1;
					else
						new_grid[i][j] = 0;
				}
				if (grid[i][j] == 1)
				{
					int num_alive = sum_num_alive(grid, i, j);
					if (num_alive < 2 || num_alive > 3)
						new_grid[i][j] = 0;
					else
						new_grid[i][j] = 1;
				}
			}
		}
		print_grid(new_grid);
		sleep(0.001);
		temp = grid;
		grid = new_grid;
		new_grid = temp;
		/*
		for (i=0; i<n; i++)
		{
			for (j=0; j<n; j++)
			{
				grid[i][j] = new_grid[i][j];
			}
		}*/
		
	}

	free(grid_cols);
	free(grid);
	free(new_grid_cols);
	free(new_grid);
	
	return 0;
}
